import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import MapView, { Heatmap } from 'react-native-maps';

export const Map = () => {

    const [points, setPoints] = useState(null);
    useEffect(() => {
        console.log("test")
        if (!points) {
            loadCoord();
        }
    })

    const loadCoord = async () => {
        const dot = await fetch('https://radian.si/for-customers/quantectum/heatmap.json')
            .then(response => response.text())
            .then(data => {
                eval(`${data}`);
            });
    }

    const eqfield_callback = (results) => {
        var field_data = [];
        var weight_count = 1805;
        for (let lat = -85; lat <= 85; lat++) {
            for (let lng = -180; lng <= 180; lng++) {
                var ter = results.features[weight_count]

                if (results.features[weight_count] == 0) //ошибка нулевых весов
                {
                    ter = ter + 0.001;
                }

                if (lng == -179) //ошибка google карт для 179 долготы
                {
                    field_data.push({
                        latitude: lat, longitude: 180,
                        weight: 0.01,
                    });
                }
                else {
                    field_data.push({
                        latitude: lat, longitude: lng,
                        weight: ter,
                    });
                }
                weight_count++
            }
        };
        setPoints(field_data)
    }


    const initialRegion = {
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
    }

    return (
        <View style={styles.container}>
            {
                points ?
                    <MapView>
                        <Heatmap
                            style={styles.map}
                            points={points}
                            opacity={0.7}
                            gradient={{ colors: ['#4682B4', 'green', 'yellow', 'red'], startPoints: [0.04, 0.1, 0.18, 0.2], colorMapSize: 2000 }}
                        />
                    </MapView> :
                    <Text>loading</Text>
            }
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    map: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    },
});
